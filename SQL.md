# STRUCTURED QUERY LANGUAGE(SQL)
* SQL is a standard language for accessing and manipulating databases. It stands for Structured Query Language.
* It is a computer language for storing, manipulating and retrieving data stored in a relational database.
* All the Relational Database Management Systems (RDMS) like MySQL, MS Access, Oracle, Sybase, Informix, Postgres and SQL Server use SQL as their standard database language.

## Applications of SQL
* It allows users to access data in the relational database management systems.
* It define the data in a database and manipulate that data.
* It create and drop databases and tables.
* It allows users to create view, stored procedure, functions in a database.
* It set permissions on tables, procedures and views.

## SQL Process
 When you are executing an SQL command for any RDBMS, the system determines the best way to carry out your request and SQL engine figures out how to interpret the task. There are various components included in this process. These components are-
1. Query Dispatcher
1. Optimization Engines
1. Classic Query Engine
1. SQL Query Engine, etc.

![Alt Text](https://www.tutorialspoint.com/sql/images/sql-architecture.jpg)


## SQL Commands
> SELECT - extracts data from a database
> 
**Syntax:** 
   
        SELECT column1, column2, ...
        FROM table_name;
       
> UPDATE - updates data in a database
> 
**Syntax:**

        UPDATE table_name
        SET column1 = value1, column2 = value2, ...
        WHERE condition;

> DELETE - deletes data from a database
> 
**Syntax:**

        DELETE FROM table_name WHERE condition;
> 
> INSERT INTO - inserts new data into a database
> 
**Syntax**

        INSERT INTO table_name (column1, column2, column3, ...)
        VALUES (value1, value2, value3, ...);

> CREATE DATABASE - creates a new database
> 
**Syntax**

         CREATE DATABASE DatabaseName;

> 
**Syntax**
> 
> CREATE TABLE - creates a new table
> 
**Syntax**

        CREATE TABLE table_name(
        column1 datatype,
        column2 datatype,
        column3 datatype,
        .....
        columnN datatype,
        PRIMARY KEY( one or more columns )
        );

> ALTER TABLE - alters or modifies an existing table
> 
**Syntax**

          ALTER TABLE table_name {ADD|DROP|MODIFY} column_name {data_ype};
> 
> DROP TABLE - deletes a table
> 
**Syntax**

        DROP TABLE table_name;
> 
> CREATE INDEX - creates an indexes of table
> 
**Syntax**

        CREATE UNIQUE INDEX index_name
        ON table_name ( column1, column2,...columnN);
> 
> DROP INDEX - deletes an index
> 
**Syntax**

        ALTER TABLE table_name
        DROP INDEX index_name;

## SQL JOIN
A JOIN clause is used to combine rows from two or more tables, based on a related column between them.

## Types of SQL JOIN
 
> INNER JOIN-This keyword selects records that have matching values in both tables.
> 
**Syntax**

        SELECT column_name(s)
        FROM table1
        INNER JOIN table2
        ON table1.column_name = table2.column_name;

![Alt Text](https://images.squarespace-cdn.com/content/v1/5732253c8a65e244fd589e4c/1464122775537-YVL7LO1L7DU54X1MC2CI/ke17ZwdGBToddI8pDm48kMjn7pTzw5xRQ4HUMBCurC5Zw-zPPgdn4jUwVcJE1ZvWMv8jMPmozsPbkt2JQVr8L3VwxMIOEK7mu3DMnwqv-Nsp2ryTI0HqTOaaUohrI8PIvqemgO4J3VrkuBnQHKRCXIkZ0MkTG3f7luW22zTUABU/image-asset.png?format=300w)

> LEFT JOIN-This keyword returns all records from the left table and the matching records from the right table.
> 
**Syntax**

        SELECT column_name(s)
        FROM table1
        LEFT JOIN table2
        ON table1.column_name = table2.column_name;

![Alt Text](https://images.squarespace-cdn.com/content/v1/5732253c8a65e244fd589e4c/1464122797709-C2CDMVSK7P4V0FNNX60B/ke17ZwdGBToddI8pDm48kMjn7pTzw5xRQ4HUMBCurC5Zw-zPPgdn4jUwVcJE1ZvWEV3Z0iVQKU6nVSfbxuXl2c1HrCktJw7NiLqI-m1RSK4p2ryTI0HqTOaaUohrI8PIO5TUUNB3eG_Kh3ocGD53-KZS67ndDu8zKC7HnauYqqk/image-asset.png?format=300w)

> RIGHT JOIN-The RIGHT JOIN keyword returns all records from the right table, and the matching records from the left table.

**Syntax**

        SELECT column_name(s)
        FROM table1
        RIGHT JOIN table2
        ON table1.column_name = table2.column_name;

![Alt Text](https://images.squarespace-cdn.com/content/v1/5732253c8a65e244fd589e4c/1464122744888-MVIUN2P80PG0YE6H12WY/ke17ZwdGBToddI8pDm48kMjn7pTzw5xRQ4HUMBCurC5Zw-zPPgdn4jUwVcJE1ZvWlExFaJyQKE1IyFzXDMUmzc1HrCktJw7NiLqI-m1RSK4p2ryTI0HqTOaaUohrI8PI-FpwTc-ucFcXUDX7aq6Z4KQhQTkyXNMGg1Q_B1dqyTU/image-asset.png?format=300w)


> FULL JOIN-This keyword returns all records when there is a match in left or right table records.
> 
**Syntax** 

        SELECT column_name(s)
        FROM table1 
        FULL OUTER JOIN table2
        ON table1.column_name = table2.column_name
        WHERE condition;

![Alt Text](https://images.squarespace-cdn.com/content/v1/5732253c8a65e244fd589e4c/1464122981217-RIYH5VL2MF1XWTU2DKVQ/ke17ZwdGBToddI8pDm48kMjn7pTzw5xRQ4HUMBCurC5Zw-zPPgdn4jUwVcJE1ZvWEV3Z0iVQKU6nVSfbxuXl2c1HrCktJw7NiLqI-m1RSK4p2ryTI0HqTOaaUohrI8PIO5TUUNB3eG_Kh3ocGD53-KZS67ndDu8zKC7HnauYqqk/image-asset.png?format=300w)


# References
|Referencs | Links
|------------ | -------------|
|Reference 1 |https://www.w3schools.com/sql/default.asp |
|Reference 2 | https://www.tutorialspoint.com/sql/index.htm|
|Reference 3 | https://www.edureka.co/blog/sql-basics/|